#include "mainwindow.h"
#include <QApplication>
#include <QMainWindow>
#include <QColorDialog>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.setGeometry(250, 250, 600, 400);
    //QColorDialog *colorDialog = new QColorDialog();
    //w.setCenterWidget(colorDialog);
    //colorDialog->setWindowFlags(Qt::Widget);
    //colorDialog->setOptions(
    //            QColorDialog::DontUseNativeDialog | QColorDialog::NoButtons );
    
    w.show();
    return a.exec();
}
