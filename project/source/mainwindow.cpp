#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMainWindow>
#include <QColorDialog>
#include <QGraphicsScene>
#include <QGraphicsView>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    
    QColorDialog *colorDialog = new QColorDialog();

    QGraphicsScene *scene = new QGraphicsScene();
    scene->addText("Hello, world!");

    // a blue background
    scene->setBackgroundBrush(Qt::blue);

    // a gradient background
    QRadialGradient gradient(0, 0, 70);
    gradient.setSpread(QGradient::RepeatSpread);
    scene->setBackgroundBrush(gradient);

    QGraphicsView *view = new QGraphicsView(scene);
    view->show();

    ui->gridLayout->addWidget(view, 0, 1);
    ui->gridLayout->addWidget(colorDialog, 0, 2);
    
    
    colorDialog->setWindowFlags(Qt::Widget);
    colorDialog->setOptions(
                QColorDialog::DontUseNativeDialog | QColorDialog::NoButtons );

}

MainWindow::~MainWindow()
{
    delete ui;
}
